#ifndef TIFREADER_H
#define TIFREADER_H

#include "CImg.h"
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class Tif2Inr{
  unsigned char* data;
  int size;
  
  class InrHeader{
    int xdim, ydim, zdim, vdim;
    int vx, vy, vz;
    string type;
    int pixsize;
    int scale;
    string cpu;

  public:
    InrHeader(int, int, int, int, float, float, float, const string, int, int, const string);
    InrHeader();
    
    const string writeHeader();
	};

  InrHeader *inrHeader;

public:
  Tif2Inr(const string path, const string filename, const int startIdx, const int endIdx);
  ~Tif2Inr();

  unsigned char* getData(){return data;}
  void saveToInr(const string filename);
};



#endif TIFREADER_H