#include "Tif2Inr.h"
#include <sstream>
#include <iomanip>

using namespace std;
using namespace cimg_library;

Tif2Inr::InrHeader::InrHeader(int _xdim, int _ydim, int _zdim, int _vdim, float _vx,
  float _vy, float _vz, const string _type, int _pixsize, int _scale, const string _cpu = "pc"){
    xdim = _xdim, ydim = _ydim, zdim = _zdim, vdim = _vdim, vx = _vx, vy = _vy, vz = _vz, type = _type,
    pixsize = _pixsize, scale = _scale, cpu = _cpu;
    }

Tif2Inr::Tif2Inr(const string path, const string filename, const int startIdx, const int endIdx){
  CImg<unsigned char> src( (path + filename + "0000.tif").c_str() );

  int nSlice = endIdx - startIdx + 1;
  size = src.width() * src.height() * nSlice;
  data = new unsigned char[size];
  long count = 0;

  for(int i=startIdx; i<=endIdx; ++i){
    char fileIdx[30];
    sprintf(fileIdx, "%04d.tif", i);
    src.load( (path + filename + fileIdx).c_str() );
    cimg_forXY(src, x, y){
      data[count++] = src(x, y);
        }
    }

  inrHeader = new InrHeader(src.width(), src.height(), nSlice, 1, 1, 1, 1, "unsigned fixed", 8, 1);
}

const string Tif2Inr::InrHeader::writeHeader(){
  stringstream ss;
  ss << "#INRIMAGE-4#{" << endl << "XDIM=" << xdim << "YDIM=" << ydim << "ZDIM=" << zdim << "VDIM=" << vdim
     << "VX=" << vx << "VY=" << vy << "VZ=" << vz << "TYPE=" << type << "PIXSIZE=" << pixsize << "SCALE=" << scale << "CPU=" << cpu << endl;
  
  string head = ss.str();
  return head;
}

void Tif2Inr::saveToInr(const string filename){
  ofstream file( filename );
  file << setw(252) << left << inrHeader->writeHeader() << "##}\n";
  file.write( (char *)data, size);
  file.close();
}

Tif2Inr::~Tif2Inr(){
  delete inrHeader;
}